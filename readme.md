# shortest path parallel


## requirements

- own libgomp build
- include custom omp.h
- big graph files
- CUDA environment


## roadmap 24.7

- [x] dijkstra sequential
- [x] dijkstra OMP
- [x] dijkstra CUDA
- [x] graph function
- [x] comparison
- [x] docs
- [x] presentation 26.6